package model

import (
	"bitbucket.org/exgo-tech/exgo-common/model/enum"
	"bitbucket.org/exgo-tech/exgo-common/slice"
	"github.com/dgrijalva/jwt-go"
	"time"
)

type (
	// JWTCustomClaims : Custom claims
	JWTCustomClaims struct {
		UserID         string `json:"user_id"`
		Name           string `json:"name"`
		Phone          string `json:"phone"`
		Roles          string `json:"roles"`
		PermissionAPIs string `json:"permission_apis"`
		jwt.StandardClaims
	}

	// TokenInfo : infor was store in token
	TokenInfo struct {
		UserID         string                     `json:"user_id" structs:"user_id"`
		Name           string                     `json:"name" structs:"name"`
		Phone          string                     `json:"phone" structs:"phone"`
		Roles          []string                   `json:"roles" structs:"roles"`
		PermissionAPIs map[string]*PermissionAPIs `json:"permission_apis" structs:"permission_apis"`
		ExpiresAt      time.Duration              `json:"expires_at" structs:"expires_at"`
	}

	// PermissionAPIs : list of APIs which were allowed and forbiddened
	PermissionAPIs struct {
		AllowAPIs     []string `json:"allow_apis" structs:"allow_apis"`
		ForbiddenAPIs []string `json:"forbidden_apis" structs:"allow_apis"`
	}
)

// ------- Util func -------

// IsDriver checkes Token has role Driver or not
func (t TokenInfo) IsDriver() bool {
	return slice.Include(t.Roles, enum.UserRoleDriver.Str())
}

// IsShipper checkes Token has role Shipper or not
func (t TokenInfo) IsShipper() bool {
	return slice.Include(t.Roles, enum.UserRoleShipper.Str())
}

// IsAdmin checks Token has Admin role or not
func (t TokenInfo) IsAdmin() bool {
	return slice.Include(t.Roles, enum.UserRoleAdmin.Str())
}

// IsCarrier checks Token has Carrier role or not
func (t TokenInfo) IsCarrier() bool {
	return slice.Include(t.Roles, enum.UserRoleCarrier.Str())
}

// IsCustomerService checks Token has Customer Service or not
func (t TokenInfo) IsCustomerService() bool {
	return slice.Include(t.Roles, enum.UserRoleCustomerService.Str())
}

// IsViewer checks Token's type is Viewer user or not
func (t TokenInfo) IsViewer() bool {
	return slice.Include(t.Roles, enum.UserRoleViewer.Str())
}

// IsManagerUser checks Token's type is Manager user or not. Manager user is user group
func (t TokenInfo) IsManagerUser() bool {
	return t.IsAdmin() || t.IsCustomerService() || t.IsViewer()
}

// GetRole will return FIRST role of user
func (t TokenInfo) GetRole() string {
	return t.Roles[0]
}

// GetRoles will return roles of user
func (t TokenInfo) GetRoles() []string {
	return t.Roles
}
