package auth

import (
	"bitbucket.org/exgo-tech/exgo-security/model"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"os"
	"strings"
	"time"
)

type (
	// JWTI defines func can use for authorization
	JWTI interface {
		GenerateToken(model.TokenInfo) string
	}

	// JWT is reciever struct
	JWT struct{}
)

// Seperator for split, join slice
const Seperator string = "|"

// GenerateToken : create token when login
func (j *JWT) GenerateToken(info model.TokenInfo) (token string, errMsg string) {
	// Set info of user to token
	permissionAPIs, _ := json.Marshal(info.PermissionAPIs)
	claims := &model.JWTCustomClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(info.ExpiresAt).Unix(),
		},
	}

	// Set data for return
	if claims.UserID, errMsg = Encrypt(os.Getenv("SECRET_KEY_JWT"), info.UserID); len(errMsg) > 0 {
		return
	}
	if claims.Name, errMsg = Encrypt(os.Getenv("SECRET_KEY_JWT"), info.Name); len(errMsg) > 0 {
		return
	}
	if claims.Phone, errMsg = Encrypt(os.Getenv("SECRET_KEY_JWT"), info.Phone); len(errMsg) > 0 {
		return
	}
	if claims.Roles, errMsg = Encrypt(os.Getenv("SECRET_KEY_JWT"), strings.Join(info.Roles, Seperator)); len(errMsg) > 0 {
		return
	}
	if claims.PermissionAPIs, errMsg = Encrypt(os.Getenv("SECRET_KEY_JWT"), string(permissionAPIs)); len(errMsg) > 0 {
		return
	}

	// Create a token with our secret key
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(os.Getenv("SECRET_KEY_JWT")))
	if token == "" {
		errMsg = fmt.Sprintf("Failed to create a token for phone: %s", info.Phone)
		if err != nil {
			errMsg = fmt.Sprintf("Failed to create a token. Error: %s", err.Error())
		}
		return "", errMsg
	}

	return token, ""
}

// DecodeToken return JSON data of token
func DecodeToken(tokenI interface{}) (tokenInfo model.TokenInfo, errMsg string) {
	token := tokenI.(*jwt.Token)
	claims := token.Claims.(*model.JWTCustomClaims)
	decodedPermissions := map[string]*model.PermissionAPIs{}
	rsDecrypt, errMsg := Decrypt(os.Getenv("SECRET_KEY_JWT"), claims.PermissionAPIs)
	if len(errMsg) > 0 {
		return
	}
	err := json.Unmarshal([]byte(rsDecrypt), &decodedPermissions)
	if err != nil {
		errMsg = fmt.Sprintf("Can not decode token. Error: %+v", err)
		return tokenInfo, errMsg
	}

	// Set data for return
	if tokenInfo.UserID, errMsg = Decrypt(os.Getenv("SECRET_KEY_JWT"), claims.UserID); len(errMsg) > 0 {
		return
	}
	if tokenInfo.Name, errMsg = Decrypt(os.Getenv("SECRET_KEY_JWT"), claims.Name); len(errMsg) > 0 {
		return
	}
	if tokenInfo.Phone, errMsg = Decrypt(os.Getenv("SECRET_KEY_JWT"), claims.Phone); len(errMsg) > 0 {
		return
	}
	roles, errMsg := Decrypt(os.Getenv("SECRET_KEY_JWT"), claims.Roles)
	if len(errMsg) > 0 {
		return
	}
	tokenInfo.Roles = strings.Split(roles, Seperator)
	tokenInfo.PermissionAPIs = decodedPermissions
	tokenInfo.ExpiresAt = time.Duration(claims.ExpiresAt)
	return tokenInfo, ""
}

// GetJWTConfig is configure middleware with the custom claims type
func GetJWTConfig() echo.MiddlewareFunc {
	JWTConfig := JWTWithConfig(
		JWTConfig{
			Claims:     &model.JWTCustomClaims{},
			SigningKey: []byte(os.Getenv("SECRET_KEY_JWT")),
		},
	)
	return JWTConfig
}
