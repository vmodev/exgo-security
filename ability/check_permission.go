package ability

import (
	"reflect"
	"strings"

	"bitbucket.org/exgo-tech/exgo-common/slice"
	model "bitbucket.org/exgo-tech/exgo-security/model"
)

// CanAccess check permission of token base on APIName
func CanAccess(token model.TokenInfo, role string, APIName string) bool {
	if strings.ToLower(role) == "all" {
		v := reflect.ValueOf(token.PermissionAPIs)
		for _, key := range v.MapKeys() {
			if CanAccess(token, key.String(), APIName) {
				return true
			}
		}
		return false
	}
	pm := token.PermissionAPIs[strings.ToLower(role)]
	if pm == nil {
		return false
	}
	if slice.Include(pm.ForbiddenAPIs, "All") {
		return slice.Include(pm.AllowAPIs, APIName)
	}
	if slice.Include(pm.AllowAPIs, "All") {
		return !slice.Include(pm.ForbiddenAPIs, APIName)
	}
	return slice.Include(pm.AllowAPIs, APIName)
}
