module bitbucket.org/exgo-tech/exgo-security

go 1.12

require (
	bitbucket.org/exgo-tech/exgo-common v1.0.6
	bitbucket.org/exgo-tech/exgo-redis v1.0.6 // indirect
	firebase.google.com/go v3.8.0+incompatible // indirect
	github.com/aws/aws-lambda-go v1.11.1 // indirect
	github.com/aws/aws-sdk-go v1.19.41 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/labstack/echo/v4 v4.1.5
	github.com/nyaruka/phonenumbers v1.0.42 // indirect
	github.com/qor/admin v0.0.0-20190525133731-6329506ec305 // indirect
	github.com/qor/i18n v0.0.0-20181014061908-f7206d223bcd
	go.opencensus.io v0.22.0 // indirect
	golang.org/x/mod v0.1.0 // indirect
	golang.org/x/sys v0.0.0-20190531175056-4c3a928424d2 // indirect
	golang.org/x/tools v0.0.0-20190601110225-0abef6e9ecb8 // indirect
	google.golang.org/genproto v0.0.0-20190530194941-fb225487d101 // indirect
	honnef.co/go/tools v0.0.0-20190531162725-42df64e2171a // indirect
)
